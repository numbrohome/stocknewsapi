# Stock News API

It's an example of API that uses external resources to provide information about stock exchange. Application was created with PHP7, Symfony3 and it's tested with PHP Unit.

### To install project

```sh
$ composer install
```

### Exposed end-points

```sh
/api/companies
/api/companies/{TICKER_CODE}
```

### To run PHP Unit test

```sh
$ phpunit
```
