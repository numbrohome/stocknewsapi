<?php

namespace ApiBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="company", repositoryClass="ApiBundle\Repository\ODM\CompanyRepository")
 */
class Company implements \JsonSerializable
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @MongoDB\Field("string")
     */
    protected $name;

    /**
     * @MongoDB\Field("string")
     */
    protected $tickerCode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTickerCode()
    {
        return $this->tickerCode;
    }

    /**
     * @param string $tickerCode
     */
    public function setTickerCode($tickerCode)
    {
        $this->tickerCode = $tickerCode;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    function toArray()
    {
        return [
            'name' => $this->getName(),
            'tickerCode' => $this->getTickerCode()
        ];
    }
}