<?php

namespace ApiBundle\Service\StockApiClient;

use GuzzleHttp\Psr7\Stream as GuzzleStream;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use GuzzleHttp\Client as GuzzleClient;

class StockApiClientTest extends \PHPUnit_Framework_TestCase
{
    private $client;
    private $restClientStub;

    public function setUp()
    {
        $this->restClientStub = $this->getMockBuilder(GuzzleClient::class)
            ->getMock();

        $this->client = new StockApiClient(
            $this->restClientStub,
            ['stock_price' => 'http://test.com/{ticker_code}']
        );
    }

    /**
     * @covers StockApiClient::getStock
     */
    public function testGetStock()
    {
        $tickerCode = 'ABCD';

        $stockPrice = [
            'tickerCode' => $tickerCode,
            'latestPrice' => 54407,
            'priceUnits' => 'GBP:pence',
            'asOf' => '2015-05-06T15:05:59.912Z',
            'storyFeedUrl' => 'http:/test.com/story-feed/8271',
        ];

        $storyFeed = [
            [
                'id' => 1,
                'headline' => 'Make up a headline',
                'body' => 'Body with enough words to trigger sentiment analysis',
            ],
            [
                'id' => 2,
                'headline' => 'Make up a headline',
                'body' => 'Body with enough words to trigger sentiment analysis',
            ],
        ];

        $this->restClientStub->expects($this->at(0))
            ->method('request')
            ->with('GET', sprintf('http://test.com/%s', $tickerCode))
            ->willReturn($this->getResponseMock(json_encode($stockPrice)));

        $this->restClientStub->expects($this->at(1))
            ->method('request')
            ->with('GET', 'http:/test.com/story-feed/8271')
            ->willReturn($this->getResponseMock(json_encode($storyFeed)));

        $expected = array_merge($stockPrice, ['storyFeed' => $storyFeed]);

        $this->assertEquals($expected, $this->client->getStock($tickerCode));
    }

    private function getResponseMock($response, $statusCode=200)
    {
        $stream = $this->getMockBuilder(GuzzleStream::class)
            ->disableOriginalConstructor()
            ->setMethods(['getContents'])
            ->getMock();
        $stream->expects($this->once())
            ->method('getContents')
            ->willReturn($response);

        $response = $this->getMockBuilder(GuzzleResponse::class)
            ->disableOriginalConstructor()
            ->setMethods(['getStatusCode', 'getBody'])
            ->getMock();
        $response->expects($this->once())
            ->method('getStatusCode')
            ->willReturn($statusCode);
        $response->expects($this->once())
            ->method('getBody')
            ->willReturn($stream);

        return $response;
    }
}