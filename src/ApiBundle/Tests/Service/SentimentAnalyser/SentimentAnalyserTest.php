<?php

namespace ApiBundle\Service\SentimentAnalyser;

class SentimentAnalyserTest extends \PHPUnit_Framework_TestCase
{
    private $sentimentAnalyser;

    public function setUp()
    {
        $positiveWords = ['positive', 'success', 'grow', 'gains', 'happy', 'healthy'];
        $negativeWords = ['disappointing', 'concerns', 'decline', 'drag', 'slump', 'feared'];

        $this->sentimentAnalyser = new SentimentAnalyser($positiveWords, $negativeWords);
    }

    /**
     * @covers SentimentAnalyser::analyse
     */
    public function testAnalyseWithNeutralText()
    {
        $text = 'It is a happy text.';

        $this->assertEquals(SentimentAnalyser::POSITIVITY_NEUTRAL, $this->sentimentAnalyser->analyse($text));
    }

    /**
     * @covers SentimentAnalyser::analyse
     */
    public function testAnalyseWithNegativeText()
    {
        $text = 'It is a disappointing text.';

        $this->assertEquals(SentimentAnalyser::POSITIVITY_NEGATIVE, $this->sentimentAnalyser->analyse($text));
    }

    /**
     * @covers SentimentAnalyser::analyse
     */
    public function testAnalyseWithPositiveText()
    {
        $text = 'It is a happy text with positive content.';

        $this->assertEquals(SentimentAnalyser::POSITIVITY_POSITIVE, $this->sentimentAnalyser->analyse($text));
    }
}