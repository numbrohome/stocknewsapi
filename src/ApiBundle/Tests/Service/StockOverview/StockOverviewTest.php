<?php

namespace ApiBundle\Service\StockOverview;

use ApiBundle\Repository\ODM\CompanyRepository;
use ApiBundle\Service\StockApiClient\StockApiClient;
use ApiBundle\Service\SentimentAnalyser\SentimentAnalyser;
use ApiBundle\Document\Company;

class StockOverviewTestextends extends \PHPUnit_Framework_TestCase
{
    private $service;
    private $stockApiClientStub;
    private $companyRepositoryStub;
    private $sentimentAnalyserStub;

    public function setUp()
    {
        $this->stockApiClientStub = $this->getMockBuilder(StockApiClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->companyRepositoryStub = $this->getMockBuilder(CompanyRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->sentimentAnalyserStub = $this->getMockBuilder(SentimentAnalyser::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->service = new StockOverview(
            $this->stockApiClientStub,
            $this->companyRepositoryStub,
            $this->sentimentAnalyserStub
        );
    }

    /**
     * @covers StockOverview::getCompanyOverview
     */
    public function testGetCompanyOverview()
    {
        $tickerCode = 'ABCD';
        $storyText = 'TEST STORY';

        $companyStub = $this->getMockBuilder(Company::class)
            ->setMethods(['toArray'])
            ->getMock();
        $companyStub->expects($this->once())
            ->method('toArray')
            ->willReturn(['name' => 'Test Company Name', 'tickerCode' => $tickerCode]);

        $this->companyRepositoryStub->expects($this->once())
            ->method('findByTickerCode')
            ->with($tickerCode)
            ->willReturn($companyStub);

        $this->stockApiClientStub->expects($this->once())
            ->method('getStock')
            ->with($tickerCode)
            ->willReturn([
                'tickerCode' => $tickerCode,
                'storyFeed' => [['body' => $storyText]]
            ]);

        $this->sentimentAnalyserStub->expects($this->once())
            ->method('analyse')
            ->with($storyText)
            ->willReturn('neutral');

        $expected = [
            'name' => 'Test Company Name',
            'tickerCode' => $tickerCode,
            'storyFeed' => [
                [
                    'body' => $storyText,
                    'positivity' => 'neutral'
                ]
            ]
        ];

        $this->assertEquals($expected, $this->service->getCompanyOverview($tickerCode));
    }

    /**
     * @covers StockOverview::getCompanyOverview
     */
    public function testGetCompanyOverviewWhenCompanyDoesntExist()
    {
        $tickerCode = 'ABCD';

        $this->companyRepositoryStub->expects($this->once())
            ->method('findByTickerCode')
            ->with($tickerCode)
            ->willReturn(null);

        $this->stockApiClientStub->expects($this->never())
            ->method('getStock');

        $this->assertNull($this->service->getCompanyOverview($tickerCode));
    }
}