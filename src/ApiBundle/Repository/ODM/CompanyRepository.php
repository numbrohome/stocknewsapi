<?php

namespace ApiBundle\Repository\ODM;

use Doctrine\ODM\MongoDB\DocumentRepository;

/**
 * Class CompanyRepository
 *
 * @package ApiBundle\Repository
 */
class CompanyRepository extends DocumentRepository
{
    /**
     * @param String $tickerCode Company ticker code.
     *
     * @return object
     */
    public function findByTickerCode(String $tickerCode)
    {
        return $this->findOneBy([
            'tickerCode' => $tickerCode
        ]);
    }
}