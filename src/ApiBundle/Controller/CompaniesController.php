<?php

namespace ApiBundle\Controller;

/**
 * Class CompaniesController
 *
 * @package ApiBundle\Controller
 */
class CompaniesController extends BaseController
{
    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function indexAction()
    {
        $companies = $this->get('api.odm_company_repository')->findAll();

        return $this->response($companies);
    }

    /**
     * @param $tickerCode
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAction($tickerCode)
    {
        $company = $this->get('api.stock_overview')->getCompanyOverview($tickerCode);

        if ($company === null) {
            return $this->resourceNotFound();
        }

        return $this->response($company);
    }
}
