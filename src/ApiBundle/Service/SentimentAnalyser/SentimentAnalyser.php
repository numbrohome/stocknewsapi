<?php

namespace ApiBundle\Service\SentimentAnalyser;

/**
 * Class SentimentAnalyser
 *
 * @package ApiBundle\Service\SentimentAnalyser
 */
class SentimentAnalyser
{
    const POSITIVITY_NEUTRAL = 'neutral';
    const POSITIVITY_NEGATIVE = 'negative';
    const POSITIVITY_POSITIVE = 'positive';

    private $positiveWords;
    private $negativeWords;

    /**
     * SentimentAnalyser constructor.
     *
     * @param array $positiveWords List of positive words.
     * @param array $negativeWords List of negative words.
     */
    public function __construct(array $positiveWords, array $negativeWords)
    {
        $this->positiveWords = $positiveWords;
        $this->negativeWords = $negativeWords;
    }

    /**
     * @param String $text
     *
     * @return string
     */
    public function analyse(String $text)
    {
        list($positiveWordsCount, $negativeWordsCount) = $this->countWords($text);
        $positivity = $positiveWordsCount - $negativeWordsCount;

        switch (true) {
            case ($positivity >= 0 && $positivity < 2):
                return self::POSITIVITY_NEUTRAL;
            case ($positivity < 0):
                return self::POSITIVITY_NEGATIVE;
            case ($positivity > 1):
                return self::POSITIVITY_POSITIVE;
        }
    }

    /**
     * @param String $text  Searchable text.
     *
     * @return array An array with number of positive words and number of negative words.
     */
    private function countWords($text)
    {
        $positiveWordsCount = 0;
        $negativeWordsCount = 0;

        $words = array_count_values(str_word_count(mb_strtolower($text), 1));

        foreach ($words as $word => $count) {
            switch (true) {
                case (in_array($word, $this->positiveWords)):
                    $positiveWordsCount += $count;
                    break;
                case (in_array($word, $this->negativeWords)):
                    $negativeWordsCount += $count;
                    break;
            }
        }

        return [$positiveWordsCount, $negativeWordsCount];
    }
}