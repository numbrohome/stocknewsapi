<?php

namespace ApiBundle\Service\StockApiClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class StockApiClient
 *
 * @package ApiBundle\Service\StockApiClient
 */
class StockApiClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $endpoints;

    /**
     * StockApiClient constructor.
     *
     * @param Client $client    REST Client.
     * @param array  $endpoints Map with named endpoints.
     */
    public function __construct(Client $client, array $endpoints)
    {
        $this->client = $client;
        $this->endpoints = $endpoints;
    }

    /**
     * @param String $tickerCode The ticket code.
     *
     * @return array
     */
    public function getStock(String $tickerCode)
    {
        $url = str_replace('{ticker_code}', $tickerCode, $this->endpoints['stock_price']);

        $stock = $this->sendRequest($url) ?? [];

        if (isset($stock['storyFeedUrl'])) {
            $stock['storyFeed'] = $this->sendRequest($stock['storyFeedUrl']);
        }

        return $stock;
    }

    /**
     * @param String $url    The url that will be used for http request.
     * @param String $method HTTP method.
     *
     * @return mixed
     */
    private function sendRequest($url, $method='GET')
    {
        try {
            $response = $this->client->request($method, $url);
        } catch (ClientException $e) {
            return null;
        }

        if ($response->getStatusCode() !== 200) {
            return null;
        }

        return json_decode($response->getBody()->getContents(), true);
    }
}