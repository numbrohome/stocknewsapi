<?php

namespace ApiBundle\Service\StockOverview;

use ApiBundle\Repository\ODM\CompanyRepository;
use ApiBundle\Service\SentimentAnalyser\SentimentAnalyser;
use ApiBundle\Service\StockApiClient\StockApiClient;

/**
 * Class StockOverview
 *
 * @package ApiBundle\Service\StockOverview
 */
class StockOverview
{
    /**
     * @var StockApiClient
     */
    private $apiClient;

    /**
     * @var CompanyRepository
     */
    private $companyRepository;

    /**
     * @var SentimentAnalyser
     */
    private $sentimentAnalyser;

    /**
     * StockOverview constructor.
     *
     * @param StockApiClient    $apiClient
     * @param CompanyRepository $companyRepository
     * @param SentimentAnalyser $sentimentAnalyser
     */
    public function __construct(
        StockApiClient $apiClient,
        CompanyRepository $companyRepository,
        SentimentAnalyser $sentimentAnalyser
    ) {
        $this->apiClient = $apiClient;
        $this->companyRepository = $companyRepository;
        $this->sentimentAnalyser = $sentimentAnalyser;
    }

    /**
     * @param String $tickerCode
     *
     * @return array|null
     */
    public function getCompanyOverview(String $tickerCode)
    {
        $company = $this->companyRepository->findByTickerCode($tickerCode);

        if ($company === null) {
            return null;
        }

        $stockDetails = $this->apiClient->getStock($tickerCode);
        $this->analyseStock($stockDetails);

        return array_merge($company->toArray(), $stockDetails);
    }

    /**
     * @param array $stockDetails
     */
    private function analyseStock(array &$stockDetails)
    {
        if (!isset($stockDetails['storyFeed'])) {
            return;
        }

        foreach($stockDetails['storyFeed'] as &$story) {
            $story['positivity'] = $this->sentimentAnalyser->analyse($story['body']);
        }
    }
}